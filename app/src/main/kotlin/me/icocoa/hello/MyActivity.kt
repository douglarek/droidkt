package me.icocoa.hello

import android.content.Intent
import android.os.Bundle
import android.os.Debug
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import android.widget.ListView
import org.jetbrains.anko.find
import org.jetbrains.anko.findOptional

class MyActivity : AppCompatActivity() {

    private lateinit var list: ListView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my)

        val toolbar = findOptional<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        val fab = findOptional<FloatingActionButton>(R.id.fab)
        fab?.setOnClickListener { view -> Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show() }

        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayOf(HTTP_APPLICATION, BARCODE_APPLICATION))
        list = find(R.id.list_view)
        list.adapter = adapter
        list.setOnItemClickListener { parent, view, position, id ->
            when (adapter.getItem(position)) {
                HTTP_APPLICATION -> startActivity(Intent(applicationContext, HttpActivity::class.java))
                BARCODE_APPLICATION -> startActivity(Intent(applicationContext, ScannerActivity::class.java))
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        // calls after onStart method.
        super.onRestoreInstanceState(savedInstanceState)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_my, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        super.onDestroy()
        Debug.stopMethodTracing()
    }

    companion object {
        private val HTTP_APPLICATION = "http"
        private val BARCODE_APPLICATION = "barcode"
    }
}