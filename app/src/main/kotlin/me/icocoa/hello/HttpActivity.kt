package me.icocoa.hello

import android.app.ProgressDialog
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.method.ScrollingMovementMethod
import android.view.View
import android.widget.TextView
import okhttp3.OkHttpClient
import okhttp3.Request
import org.jetbrains.anko.*
import java.io.IOException

class HttpActivity : AppCompatActivity() {
    private lateinit var httpResponse: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_http)
        val toolbar = findOptional<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        val fab = findOptional<FloatingActionButton>(R.id.fab)
        fab?.setOnClickListener { view -> Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show() }
        fab?.visibility = View.INVISIBLE

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        httpResponse = find<TextView>(R.id.http_response)
        httpResponse.movementMethod = ScrollingMovementMethod.getInstance()
    }

    fun httpApiRequest(@Suppress("UNUSED_PARAMETER") v: View) {
        val progressDialog = ProgressDialog(this@HttpActivity)
        progressDialog.setCanceledOnTouchOutside(false) // do not cancel when touch screen
        progressDialog.show()

        val okHttpClient = OkHttpClient()
        progressDialog.setOnCancelListener { okHttpClient.dispatcher().cancelAll() }

        doAsync {
            val request = Request.Builder().url("https://api.github.com/").get().build()
            try {
                val str = okHttpClient.newCall(request).execute().body().string()
                uiThread { httpResponse.text = str }
            } catch (ex: IOException) {
                uiThread { toast(ex.message.orEmpty()) }
            } finally {
                uiThread { progressDialog.dismiss() }
            }
        }
    }
}
