package me.icocoa.hello

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import com.google.zxing.Result
import me.dm7.barcodescanner.zxing.ZXingScannerView
import org.jetbrains.anko.longToast
import org.jetbrains.anko.toast

class ScannerActivity : AppCompatActivity(), ZXingScannerView.ResultHandler {
    private lateinit var mScanner: ZXingScannerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mScanner = ZXingScannerView(this)
        setContentView(mScanner)
    }

    override fun onStart() {
        super.onStart()
        val perm = Manifest.permission.CAMERA
        if (ContextCompat.checkSelfPermission(this, perm) !== PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, perm)) {
                ActivityCompat.requestPermissions(this, arrayOf<String>(perm), PERMISSIONS_REQUEST_CAMERA_CODE)
            } else {
                longToast(resources.getString(R.string.camera_perm_needed))
                finish()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        mScanner.setResultHandler(this)
        mScanner.startCamera()
    }

    override fun onPause() {
        super.onPause()
        mScanner.stopCamera()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSIONS_REQUEST_CAMERA_CODE -> {
                if (grantResults.isEmpty() || grantResults.first() != PackageManager.PERMISSION_GRANTED) {
                    toast("Bye bye")
                    finish()
                }
            }
        }
    }

    override fun handleResult(result: Result?) {
        toast(arrayOf<String?>(result?.text.orEmpty(), result?.barcodeFormat?.toString()).joinToString())
        mScanner.resumeCameraPreview(this)
    }

    companion object {
        private val PERMISSIONS_REQUEST_CAMERA_CODE = 0
    }
}